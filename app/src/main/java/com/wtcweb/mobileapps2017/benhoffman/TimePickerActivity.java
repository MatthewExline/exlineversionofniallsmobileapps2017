package com.wtcweb.mobileapps2017.benhoffman;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.TimePicker;
import android.widget.Toast;

import com.wtcweb.mobileapps2017.R;

public class TimePickerActivity extends AppCompatActivity {

    //This demo shows how to use a TimePicker to select a time
    //It will take the time you set and toast it to the screen

    TimePicker tpTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_picker);
        tpTime = (TimePicker) findViewById(R.id.tpTime);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.activity_time_picker, menu);
        return true;
    }

    public void displayTime(View v) {
        String time = tpTime.getCurrentHour() + ":" + tpTime.getCurrentMinute();
        Toast.makeText(this, time, Toast.LENGTH_SHORT).show();
    }
}
